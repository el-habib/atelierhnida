package ac.ma.emi.agl5;

import static org.junit.Assert.assertTrue;
import ac.ma.emi.agl5.App;
import org.junit.Test;

/**
 * Unit test for simple App.
 */
public class AppTest 
{
    /**
     * Rigorous Test :-)
     */
    App app = new App();
    @Test
    public void shouldAnswerWithTrue()
    {
        assertTrue( true );
    }
    @Test
    public void shouldConcatUnderscore()
    {
        assertTrue( app.getName("amazzal").equals("amazzal_") );
        assertTrue( app.getName("").equals("_") );
    }
}
